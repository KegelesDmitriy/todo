//
//  Created by Dmitriy Kegeles
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Action

class TodoListViewmodel {
    var bag = DisposeBag()
    private let api: TodoProtocol.Type
    let errorSubject = PublishSubject<Error>()
    
    var source = BehaviorRelay<[Todo]>(value: [])
    
    init(api: TodoProtocol.Type) {
        self.api = api
        
        fetchAll()
    }
    
    //MARK: ACTIONS
    func onCellButtonTapped(model: Todo) -> CocoaAction {
        return CocoaAction {
            self.update(model: model)
            return Observable.empty()
        }
    }
    
    
    //MARK: HTTP
    func fetchAll() {
        self.api.all()
            .catchError({ (error) in
                self.errorSubject.onNext(error)
                return Observable.empty()
            })
            .bind(to: source)
            .disposed(by: bag)
    }
    
    func addNew(title:String) {
        let model = Todo()
        model.title = title
        self.api.new(model: model)
            .catchError({ (error) in
                self.errorSubject.onNext(error)
                return Observable.empty()
            })
            .subscribe(onNext: { (model) in
                var source = self.source.value
                source.append(model)
                self.source.accept(source)
            })
            .disposed(by: bag)
    }
    
    func update(model:Todo) {
        model.completed = !model.completed
        return self.api.update(model: model)
            .catchError({ (error) in
                self.errorSubject.onNext(error)
                return Observable.empty()
            })
            .subscribe(onNext: { (model) in
                var source = self.source.value
                if let index = self.source.value.index(of: model) {
                    source[index] = model
                    self.source.accept(source)
                }
            })
            .disposed(by: bag)
    }
    
    func delete(model:Todo) {
        self.api.delete(id: model.id)
            .catchError({ (error) in
                self.errorSubject.onNext(error)
                return Observable.empty()
            })
            .subscribe(onNext: { (_) in
                var source = self.source.value
                if let index = self.source.value.index(of: model) {
                    source.remove(at: index)
                    self.source.accept(source)
                }
            })
            .disposed(by: bag)
    }
    
}
