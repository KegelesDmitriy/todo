//
//  Created by Dmitriy Kegeles
//  Copyright © 2019 . All rights reserved.
//

import UIKit
import Action


class TodoCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    var todo: Todo?
    
    
    func updateCell(todo: Todo, action: CocoaAction) {
        self.todo = todo
        actionButton.rx.action = action
        
        self.titleLabel.text = todo.title
        if todo.completed {
            actionButton.setTitle("Complete", for: .normal)
        } else {
            actionButton.setTitle("Uncompleted", for: .normal)
        }
    }
    
}
