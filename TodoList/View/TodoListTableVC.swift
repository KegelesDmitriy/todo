//
//  Created by Dmitriy Kegeles
//  Copyright © 2019 . All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class TodoListTableVC: UITableViewController {
    var viewModel = TodoListViewmodel(api: TodoAPI.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindUI()
    }
    
    func bindUI(){
        self.viewModel.source.subscribe(onNext: { (model) in
            self.tableView.reloadData()
        })
            .disposed(by: viewModel.bag)
        
        viewModel.errorSubject.subscribe(onNext: { error in
            self.showAlert(title: "Error", message: "\(error)")
        })
            .disposed(by: viewModel.bag)
    }
    
    @IBAction func add(_ sender: Any) {
        let alertController = UIAlertController(title: "Add New Item", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Task"
        }
        let saveAction = UIAlertAction(title: "Add", style: .default) { (alertAction) in
            let textField = alertController.textFields![0]
            self.viewModel.addNew(title: textField.text!)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {_ in})
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}

//UITableViewDelegate, UITableViewDataSource
extension TodoListTableVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.source.value.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TodoCell = tableView.dequeueReusableCell(withIdentifier: "TodoCell") as! TodoCell
        let model = viewModel.source.value[indexPath.row]
        cell.updateCell(todo: model, action: viewModel.onCellButtonTapped(model: model))
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.viewModel.delete(model: self.viewModel.source.value[indexPath.row])
        }
    }
}

