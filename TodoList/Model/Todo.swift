//
//  Created by Dmitriy Kegeles
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import ObjectMapper


class Todo: ImmutableMappable {
    var id: Int
    var title: String
    var completed: Bool
    
    public init() {
        self.id = 0
        self.title = ""
        self.completed = false
    }

    required init(map: Map) throws {
        id = try map.value("id")
        title = try map.value("title")
        completed = try map.value("completed")
    }
    
    func mapping(map: Map) {
        id   >>> map["id"]
        title >>> map["title"]
        completed >>> map["completed"]
    }
}

extension Todo: Equatable {
    static func == (lhs: Todo, rhs: Todo) -> Bool {
        return lhs.id == rhs.id
    }
}
