//
//  Created by Dmitriy Kegeles
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Alamofire


struct Const {
    static let kBaseUrl:String = "http://ec2-52-32-105-2.us-west-2.compute.amazonaws.com:8080"
}

protocol TodoProtocol {
    static func all() -> Observable<[Todo]>
    static func new(model: Todo) -> Observable<Todo>
    static func update(model: Todo) -> Observable<Todo>
    static func delete(id:Int) -> Observable<Void>
}



struct TodoAPI: TodoProtocol, HTTPAPIProtocol {
    static func all() -> Observable<[Todo]> {
        let response = request(url: Address.all.fullUrl)
        return response.parse(to: Todo.self)
    }
    
    static func new(model: Todo) -> Observable<Todo> {
        let response = request(url: Address.new.fullUrl, parameters: model.toJSON(),
                               method: .post, encoding: JSONEncoding.default)
        return response.parse(to: Todo.self)
    }
    
    static func update(model: Todo) -> Observable<Todo> {
        let response = request(url: Address.update.fill(withValues: ["item_id":String(model.id)]).fullUrl,
                               parameters: model.toJSON(),
                               method:.put, encoding: JSONEncoding.default
        )
        return response.parse(to: Todo.self)
    }
    
    static func delete(id:Int) -> Observable<Void> {
        let response = request(url: Address.delete.fill(withValues: ["item_id":String(id)]).fullUrl,
                               method:.delete)
        return response.flatMap { _ in Observable.just(()) }
    }
    
    struct Address {
        static let all = "/all"
        static let new = "/new"
        static let update = "/update/{item_id}"
        static let delete = "/delete/{item_id}"
    }
}




