//
//  Created by Dmitriy Kegeles
//  Copyright © 2019 . All rights reserved.
//

import RxSwift
import RxCocoa
import Alamofire
import ObjectMapper



enum Errors: Error {
    case requestFailed
    case requestFailedWithCode(code:Int)
    case requestFailedWithString(message:String)
    case encodingFailed(_: Any)
    case parsingFailed
    case failed
}


protocol HTTPAPIProtocol {}
extension HTTPAPIProtocol {
    
    static func request(url: URL, parameters: [String: Any] = [:], method:HTTPMethod = .get,
                        encoding:ParameterEncoding = URLEncoding.httpBody) -> Observable<Any> {
        
//        print(url.absoluteString)
        
        return Observable.create { observer in
            
            
            let request = Alamofire.request(url,
                                            method: method,
                                            parameters: parameters,
                                            encoding: encoding,
                                            headers: nil)
            request.responseJSON { response in
                
                let urlStr = response.request?.url?.absoluteString ?? "Network error"
                
                guard let statusCode = response.response?.statusCode else{
                    return observer.onError(Errors.requestFailedWithString(message: "\(urlStr)"))
                }
                
                guard (statusCode >= 200 && statusCode < 300 ) else {
                    if let error = response.error {
                        observer.onError(Errors.requestFailedWithString(message: "\(urlStr), \(String(describing:error)), _code_\(statusCode)") )
                    }
                    else{
                        observer.onError(Errors.requestFailedWithString(message: "\(urlStr), _code_\(statusCode)"))
                    }
                    return
                }
                
                if case .success(let data) = response.result {
                    observer.onNext(data)
                }
                else{
                    observer.onNext(())
                }
                
                observer.onCompleted()
            }
            
            return Disposables.create {
                request.cancel()
            }
            
        }
    }
    
}


public extension ObservableType {
    func parse<T:ImmutableMappable>(to type: T.Type) -> Observable<T> {
        return self.flatMap { responseData -> Observable<T> in
            if let responseData = responseData as? [String : Any] {
                guard let model = try? T.init(JSON: responseData) else {
                    throw Errors.parsingFailed
                }
                return Observable.just(model)
            }
            throw Errors.parsingFailed
        }
    }
    
    func parse<T:ImmutableMappable>(to type: T.Type, ignoreEmpty:Bool = false) -> Observable<[T]> {
        return self.map { responseData -> [T] in
            if let responseArray = responseData as? [Any] {
                let arr =  try responseArray.map { value -> T in
                    guard let dict = value as? [String : Any],
                        let model = try? T.init(JSON: dict) else {
                            throw Errors.parsingFailed
                    }
                    return model
                }
                if ignoreEmpty == false{
                    if arr.count == 0 { throw Errors.parsingFailed }
                }
                return arr
                
            }
            throw Errors.parsingFailed
        }
    }
    
}




