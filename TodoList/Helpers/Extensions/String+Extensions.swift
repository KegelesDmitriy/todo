//
//  Created by Dmitriy Kegeles
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import UIKit


extension String {
    func fill(withValues dict:[String: Any?]?) -> String{
        guard let params = dict else {
            return self
        }
        var finalString = self
        params.forEach { (arg) in
            if let unwrappedValue = arg.value {
                finalString = finalString.replacingOccurrences(of: "{\(arg.key)}",
                    with: String(describing: unwrappedValue))
            }
        }
        finalString = finalString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        return finalString
    }

    var toUrl: URL {
        return URL(string: self) ?? URL(string: "urlerror")!
    }
    var fullUrl: URL {
        return Const.kBaseUrl.appending(self).toUrl
    }
}







extension UIViewController {
    
    func showAlert(title: String, message: String, completion:((_ result:Bool)->())? = nil ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let btn = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (action) -> Void in
            completion?(true)
        }
        alert.addAction(btn)
        self.present(alert, animated: true, completion: nil)
    }
    
}
